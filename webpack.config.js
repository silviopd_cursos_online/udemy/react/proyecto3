const path = require("path")
const HtmlWebpackPlugin = require("html-webpack-plugin")
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin")

const HOST = process.env.HOST
const PORT = process.env.PORT || Number(process.env.PORT)

module.exports = {
	entry: path.join(__dirname, "src", "index.js"),
	output: {
		path: path.join(__dirname, "build"),
		filename: "bundle.js",
		publicPath: "./"
	},
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				use: {
					loader: "babel-loader"
				}
			},
			{
				test: /\.(css|scss)$/,
				use: [
					MiniCssExtractPlugin.loader,
					"css-loader",
					"postcss-loader",
					"sass-loader"
				]
			},
			{
				test: /\.(jpg|jpeg|png|gif|mp3|svg)$/,
				use: [
					{
						loader: "file-loader",
						options: {
							name: "[path][name]-[hash:8].[ext]"
						}
					}
				]
			}
		]
	},
	devServer: {
		publicPath: "/",
		historyApiFallback: true,
		host: HOST,
		port: PORT
	},
	optimization: {
		minimizer: [new OptimizeCSSAssetsPlugin({})]
	},
	plugins: [
		new HtmlWebpackPlugin({
			filename: "index.html",
			template: path.join(__dirname, "src", "index.html"),
			minify: {
				removeAttributeQuotes: true,
				collapseWhitespace: true,
				html5: true,
				removeComments: true,
				removeEmptyAttributes: true,
				minifyCSS: true
			}
		}),
		new MiniCssExtractPlugin({
			filename: "[name].css",
			chunkFilename: "[id].css"
		})
	]
}
